# Mask files for LHC and HL-LHC

**Contributors:** R. De Maria, S. Fartoukh, M. Giovannozzi, G. Iadarola, Y. Papaphilippou, D. Pellegrini, G. Sterbini, F. Van Der Veken  

---

This page describes the structure and the usage of the LHC mask files. The contents are organized in the following categories:

 * [Introduction](#introduction)
 * [Getting started](#getting-started)
 * [Structure of the main configuration file](#structure-of-the-main-configuration-file-mainmask)
 * [Handling masked parameters](#handling-masked-parameters)

## Introduction

The mask file is a [madx](https://mad.web.cern.ch/mad/) program that prepares the LHC model used for tracking simulations, for example with [sixdesk](https://github.com/SixTrack/SixDesk)/[sixtrack](http://sixtrack.web.cern.ch/SixTrack/).
In the past the mask was monolitic long script inteleaving settings with madx code. This has been overcome by a modular version of the mask, described here, which separates settings from code. 
The settings are provided through a configuration main file (in the following called main.mask), which is edited by the user to configure the simulation. 
For most use-cases the user does not need to edit the code part, which consists in a set of modules called at the end of main.maks.

The modules are hosted in a centralized repository, available:
 - on AFS at ```/afs/cern.ch/eng/lhc/optics/simulation_tools/modules```
 - on github at https://github.com/lhcopt/lhcmask/

The modules are as much as possible independent on the machine optics or configuration (they have been tested with LHC/HL-LHC, injection/collision, round/flat).


## Running a simple example
To execute an example please create a test folder (on a machine having access to AFS).

```bash
mkdir test_folder 
```
Get an example main configuration script:
```bash
cd test_folder
cp /afs/cern.ch/eng/lhc/optics/simulation_tools/modules/examples/hl_lhc_collision/main.mask.unmasked .
```

Change the parameters to get your machine configuration, and execute with madx:
```bash
madx main.mask.unmasked
```
This example has no "masked parameters". Configuration files with masked parameters can be handled in a similar way (please refer to the [dedicated section](#handling-masked-parameters)).


# Structure of the main configuration file (main.mask)

# Handling masked parameters

For large scans some of the parameters in the main configuration script can be replaced by symbols (e.g. %CHROMATICITY) that will be set by [sixdesk](https://github.com/SixTrack/SixDesk).
In the following you find an example on how to handle this kind of files

Make a test folder and copy an example of main configuration file with masked parameters:

```
mkdir test_folder
cd test_folder
cp /afs/cern.ch/eng/lhc/optics/simulation_tools/modules/examples/hl_lhc_collision/main.mask .
```

To test quickly a main file with masked parameters, you can write their values in a file (e.g. ```parameters_for_unmask.txt```, you find a sample file in the example folder) with the following syntax:
```
%BEAM%:1
%OCT%:100
%EMIT_BEAM:2.3e-6
%NPART:2.25e11
%CHROM%:15
%XING:245
%SEEDRAN:1
```

The you can use the unmask script available in the repository to generate the unmasked version of the file:
```
cp python /afs/cern.ch/eng/lhc/optics/simulation_tools/modules/unmask.py .
python unmask.py main.mask parameters_for_unmask.txt
```      

This generates an unmasked version of the file (main.mask.unmasked), which can be executed by madx:
```
madx main.mask.unmasked
```
<!-- 
## To run an example:
```bash
cd examples/hl_lhc_collision/
python ../../unmask.py main.mask parameters_for_unmask.txt
madx main.mask.unmasked
```
or equivalently:
```bash
cd examples/hl_lhc_collision/
python ../../unmask.py main.mask parameters_for_unmask.txt --run
```

## Description

In this repository you can retrieve and contribute to improve the MADX code used to setup tracking simulations for LHC and HL-LHC.

This code is based on the work of many colleagues who shared their contributions and effort with the community for enriching this simulation framework.

We refer as *mask* the MADX input code that is the starting code for tracking simulation, FMA analysis,... The *mask* file present *masked* parameters that can be *unmasked*. Once unmasked, the mask become a regular MADX input file and can be directly run.

The proposed generic mask file has two main parts:
 1. the definition of the configuration parameters. Their value can be assigned explicitly or masked by a placeholder (to be used for SixTrack scans).
 2. the call of the madx files 
    - to load the sequence and optics without beam-beam, to define the beam crossing angle and separation, the status of the experimental magnet...
    - to level the luminosity and install/configure the BB lenses...
    - to load the beam to track and install the magnetic errors, power the octupoles, match the tunes and the chromaticities...
    - to make the final twiss and prepare the input files for SixTrack.

The separation of the configuration parameters of the mask with the MADX code aims 
- to improve the readability for the users that can focus on the input of the simulations,
- to define better interfaces for the maintenance of the MADX code.



 -->